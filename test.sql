-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 28 Okt 2019 pada 11.08
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `ttl` date NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `nohp` varchar(12) NOT NULL,
  `jeniskelamin` varchar(16) NOT NULL,
  `metode` varchar(16) NOT NULL,
  `instansi` varchar(50) NOT NULL,
  `nama_instansi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `nama`, `ttl`, `tempat_lahir`, `email`, `nohp`, `jeniskelamin`, `metode`, `instansi`, `nama_instansi`) VALUES
(20, 'Untung', '1992-09-10', 'Bandung', 'untung123@gmail.com', '08137342929', 'Laki-Laki', 'Kredit', 'Badan Khusus', 'MQ'),
(21, 'Putri Qolbi', '2002-08-20', 'Jakarta', 'aisyah00@gmail.com', '0812473821', 'Perempuan', 'Tunai', 'Negara', 'Malaysia'),
(22, 'Qodri', '2020-04-18', 'Jonggol', 'qodri@gmail.com', '08137342929', 'Laki-Laki', 'Kredit', 'Badan Khusus', 'FBI');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
