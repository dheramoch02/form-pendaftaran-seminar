<?php
// including the database connection file
include_once("config.php");

if(isset($_POST['update']))
{	

	$id = mysqli_real_escape_string($mysqli, $_POST['id']);
	
	$nama = mysqli_real_escape_string($mysqli, $_POST['nama']);
	$ttl = mysqli_real_escape_string($mysqli, $_POST['ttl']);
	$tempat_lahir = mysqli_real_escape_string($mysqli, $_POST['tempat_lahir']);
	$email = mysqli_real_escape_string($mysqli, $_POST['email']);
	$nohp = mysqli_real_escape_string($mysqli, $_POST['nohp']);
	$jeniskelamin = mysqli_real_escape_string($mysqli, $_POST['jeniskelamin']);
	$metode = mysqli_real_escape_string($mysqli, $_POST['metode']);
	$instansi = mysqli_real_escape_string($mysqli, $_POST['instansi']);
	$nama_instansi = mysqli_real_escape_string($mysqli, $_POST['nama_instansi']);
		
	// checking empty fields
	if(empty($nama) || empty($ttl) || empty($tempat_lahir) || empty($email) || empty($nohp) || empty($nama_instansi)){	
			
		if(empty($nama)) {
			echo "<font color='red'>Nama masih kosong.</font><br/>";
		}
		
		if(empty($ttl)) {
			echo "<font color='red'>Umur masih kosong.</font><br/>";
		}

		if(empty($tempat_lahir)) {
			echo "<font color='red'>tempat_lahir masih kosong.</font><br/>";
		}
		
		if(empty($email)) {
			echo "<font color='red'>Email masih kosong.</font><br/>";
		}

		if(empty($nohp)) {
			echo "<font color='red'>No Hp masih kosong.</font><br/>";
		}

		if(empty($nama_instansi)) {
			echo "<font color='red'>Nama instansi masih kosong.</font><br/>";
		}		
	} else {	
		//updating the table
		$result = mysqli_query($mysqli, "UPDATE users SET nama='$nama',ttl='$ttl',tempat_lahir='$tempat_lahir',email='$email' ,nohp='$nohp',jeniskelamin='$jeniskelamin', metode='$metode', instansi='$instansi',nama_instansi='$nama_instansi' WHERE id=$id");
		
		//redirectig to the display page. In our case, it is index.php
		header("Location: list.php");
	}
}
?>
<?php
//getting id from url
$id = $_GET['id'];

//selecting data associated with this particular id
$result = mysqli_query($mysqli, "SELECT * FROM users WHERE id=$id");

while($res = mysqli_fetch_array($result))
{
	$nama = $res['nama'];
	$ttl = $res['ttl'];
	$tempat_lahir = $res['tempat_lahir'];
	$email = $res['email'];
	$nohp = $res['nohp'];
	$jeniskelamin = $res['jeniskelamin'];
	$metode = $res['metode'];
	$instansi = $res['instansi'];
	$nama_instansi = $res['nama_instansi'];
}
?>
<html>
<head>	
	<title>Edit Data</title>
</head>

<body>
	<a href="list.php">Kembali</a>
	<br/><br/>
	
	<form name="form1" method="post" action="edit.php">
		<table width="25%" border="0">
			<tr> 
				<td>Nama</td>
				<td><input type="text" name="nama" placeholder="nama" value="<?php echo $nama;?>"></td>
			</tr>
			<tr> 
				<td>Tanggal Lahir</td>
				<td><input type="date" name="ttl" placeholder="tanggal lahir" value="<?php echo $ttl;?>"></td>
			</tr>
			<tr> 
				<td>Tempat Lahir</td>
				<td><input type="text" name="tempat_lahir" placeholder="Tempat lahir" value="<?php echo $tempat_lahir;?>"></td>
			</tr>
			<tr> 
				<td>Email</td>
				<td><input type="email" name="email" placeholder="email" value="<?php echo $email;?>"></td>
			</tr>
			<tr>
				<td>No Hp</td>
				<td><input type="tel" name="nohp" placeholder="Masukan No Hp" value="<?php echo $nohp;?>"></td>
			</tr>
			<tr>
				<td><label for="jeniskelamin">Jenis Kelamin</label>
				<td><select name="jeniskelamin" value="<?php echo $jeniskelamin; ?>">
					<option>Laki-Laki</option>
					<option>Perempuan</option>
				</select></td>
				</td>
			</tr>
			<tr>
				<td><label for="metode">Metode Pembayaran</label>
				<td><select name="metode" value="<?php echo $metode; ?>">
					<option>Tunai</option>
					<option>Kredit</option>
				</select></td>
				</td>
			</tr>
			<tr>
				<td><label for="instansi">Jenis Instansi</label>
				<td><select name="instansi" value="<?php echo $instansi; ?>">
					<option>Badan Khusus</option>
					<option>Negara</option>
					<option>Swasta</option>
				</select></td>
				</td>
			</tr>
			<tr>
				<td>Nama Instansi</td>
				<td><input type="text" name="nama_instansi" placeholder="Masukan Nama instansi" value="<?php echo $nama_instansi;?>"></td>
			</tr>
			<tr>
				<td><input type="hidden" name="id" value="<?php echo $_GET['id'];?>"></td>
				<td><input type="submit" name="update" value="Update"></td>
			</tr>
		</table>
	</form>
</body>
</html>
