<html>
<head>
	<title>Add Data</title>
</head>

<body>
<?php
//including the database connection file
include_once("config.php");

if(isset($_POST['Submit'])) {	
	$nama = mysqli_real_escape_string($mysqli, $_POST['nama']);
	$ttl = mysqli_real_escape_string($mysqli, $_POST['ttl']);
	$tempat_lahir = mysqli_real_escape_string($mysqli, $_POST['tempat_lahir']);
	$email = mysqli_real_escape_string($mysqli, $_POST['email']);
	$nohp = mysqli_real_escape_string($mysqli, $_POST['nohp']);
	$jeniskelamin = mysqli_real_escape_string($mysqli, $_POST['jeniskelamin']);
	$metode = mysqli_real_escape_string($mysqli, $_POST['metode']);
	$instansi = mysqli_real_escape_string($mysqli, $_POST['instansi']);
	$nama_instansi = mysqli_real_escape_string($mysqli, $_POST['nama_instansi']);
		
	// checking empty fields
	if(empty($nama) || empty($ttl) || empty($tempat_lahir) || empty($email) || empty($nohp) || empty($nama_instansi)){	
			
		if(empty($nama)) {
			echo "<font color='red'>Nama masih kosong.</font><br/>";
		}
		
		if(empty($ttl)) {
			echo "<font color='red'>Tanggal Lahir masih kosong.</font><br/>";
		}

		if(empty($tempat_lahir)) {
			echo "<font color='red'>tempat lahir masih kosong.</font><br/>";
		}
		
		if(empty($email)) {
			echo "<font color='red'>Email masih kosong.</font><br/>";
		}

		if(empty($nohp)) {
			echo "<font color='red'>No Hp masih kosong.</font><br/>";
		}

		if(empty($nama_instansi)) {
			echo "<font color='red'>Nama instansi masih kosong.</font><br/>";
		}
		
		//link to the previous page
		echo "<br/><a href='javascript:self.history.back();'>Go Back</a>";
	} else { 
		// if all the fields are filled (not empty) 
			
		//insert data to database	
		$result = mysqli_query($mysqli, "INSERT INTO users(nama,ttl,tempat_lahir,email,nohp,jeniskelamin,metode,instansi,nama_instansi) VALUES('$nama','$ttl','$tempat_lahir','$email', '$nohp','$jeniskelamin','$metode','$instansi','$nama_instansi')");
	
		//display success message
		echo "<font color='green'>Penambahan Data Selesai";
		echo "<br/><a href='list.php'>Lihat Hasil</a>";
	}
}
?>
</body>
</html>
