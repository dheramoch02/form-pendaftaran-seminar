<?php
//including the database connection file
include_once("config.php");

//fetching data in descending order (lastest entry first)
//$result = mysql_query("SELECT * FROM users ORDER BY id DESC"); // mysql_query is deprecated
$result = mysqli_query($mysqli, "SELECT * FROM users ORDER BY id DESC"); // using mysqli_query instead

?>

<html>
<head>	
	<title>Halaman Utama</title>
</head>

<body>
	
<a href="index.php">Home</a><br/><br/>

<form method="get" action="list.php">
	<input type="text" name="cari" placeholder="Cari...">
	<input type="submit" name="submit" value="cari">
</form>
	<table width='100%' border=0>

	<tr bgcolor='#CCCCCC'>
		<td>Nama</td>
		<td>Tanggal Lahir</td>
		<td>Tempat Lahir</td>
		<td>Email</td>
		<td>No Hp</td>
		<td>Jenis Kelamin</td>
		<td>Metode Pembayaran</td>
		<td>Jenis Instansi</td>
		<td>Nama Instansi</td>
		<td>Update</td>
	</tr>
	<?php

	//while($res = mysql_fetch_array($result)) { // mysql_fetch_array is deprecated, we need to use mysqli_fetch_array 
	while($res = mysqli_fetch_array($result)) { 		
		echo "<tr>";
		echo "<td>".$res['nama']."</td>";
		echo "<td>".$res['ttl']."</td>";
		echo "<td>".$res['tempat_lahir']."</td>";
		echo "<td>".$res['email']."</td>";
		echo "<td>".$res['nohp']."</td>";
		echo "<td>".$res['jeniskelamin']."</td>";
		echo "<td>".$res['metode']."</td>";
		echo "<td>".$res['instansi']."</td>";
		echo "<td>".$res['nama_instansi']."</td>";	
		echo "<td><a href=\"edit.php?id=$res[id]\">Edit</a> | <a href=\"delete.php?id=$res[id]\" onClick=\"return confirm('Serius Mau Hapus?')\">Hapus</a></td>";		
	}
	?>
	
	</table>
</body>
</html>
